#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see http://aka.ms/containercompat 

#FROM microsoft/dotnet:2.1-runtime-nanoserver-1809 AS base
FROM microsoft/dotnet:2.1.401-sdk-stretch AS base
WORKDIR /app

#FROM microsoft/dotnet:2.1-sdk-nanoserver-1809 AS build
FROM microsoft/dotnet:2.1.401-sdk-stretch AS build
WORKDIR /src
COPY BookProj.csproj ./
RUN dotnet restore BookProj.csproj
COPY . .
WORKDIR /src/
RUN dotnet build BookProj.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish BookProj.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "BookProj.dll"]
